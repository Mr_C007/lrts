package com.longki.demo01.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ScrollView;
import ohos.app.Context;

public class MyScrollView extends ScrollView {

    public MyScrollView(Context context) {
        super(context);
    }

    public MyScrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public MyScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
