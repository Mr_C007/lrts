package com.longki.demo01.util;

import com.longki.demo01.ResourceTable;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
/**
 * @作者 Longki
 * @时间 2020-12-28
 * @功能 弹出框自定义
 * */
public class Toast extends ToastDialog{

    public Toast(Context context) {
        super(context);
    }

    public Toast(Context context,String title) {
        super(context);
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        this.setComponent(toastLayout);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(title);
        this.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        this.setAlignment(LayoutAlignment.CENTER);
        this.show();
    }

    public static void ShowToast(Context context,String title){
        new Toast(context,title);
    }
}
