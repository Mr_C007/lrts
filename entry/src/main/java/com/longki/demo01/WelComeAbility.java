package com.longki.demo01;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import java.util.Timer;
import java.util.TimerTask;

public class WelComeAbility extends Ability {

    //几秒跳转
    private long delay = 2000;
    Timer timer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_welcome);
        timer = new Timer();
        timer.schedule(new TimerTask(){
            public void run(){
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.longki.demo01")
                        .withAbilityName("com.longki.demo01.AdvAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                setTransitionAnimation(ResourceTable.Animation_slide_right,ResourceTable.Animation_slide_left);
                terminateAbility();
            }
        },delay);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
