package com.longki.demo01.net;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class HttpConstants {

    public static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "Longki");
    //本地测试
    public static final String HEAD = "http://192.168.1.156:8888";
    //获取广告
    public static final String getAdvertisement = HEAD+"/banner/getAdvertisement";
}
