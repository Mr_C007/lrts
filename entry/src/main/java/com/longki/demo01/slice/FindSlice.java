package com.longki.demo01.slice;

import com.longki.demo01.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

/*
 * 发现界面
 * */
public class FindSlice extends Fraction {

    TabList tab_list;
    DirectionalLayout outDL;
    String[] strArr = new String[]{"阅读榜","听书榜","节目榜","漫画榜","新书榜","课程榜"};

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_ability_find_slice,container,false);
        tab_list = (TabList) component.findComponentById(ResourceTable.Id_tab_list);
        outDL = (DirectionalLayout) component.findComponentById(ResourceTable.Id_outDL);
        for(int i=0;i<strArr.length;i++){
            TabList.Tab tab = tab_list.new Tab(getContext());
            tab.setText(strArr[i]);
            tab_list.addTab(tab);
        }
        tab_list.getTabAt(0).select();
        setItem(scatter,container,0);
        tab_list.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                setItem(scatter,container,tab.getPosition());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
        return component;
    }

    private void setItem(LayoutScatter scatter,ComponentContainer container,int pos){
        //清空
        outDL.removeAllComponents();
        //判断进入不同xml
        Component component1;
        switch (pos){
            case 0:
                component1 = scatter.parse(ResourceTable.Layout_item_read,container,false);
                break;
            default:
                component1 = scatter.parse(ResourceTable.Layout_item_banner,container,false);
                break;
        }
        outDL.addComponent(component1);
    }
}
