package com.longki.demo01.slice;

import com.longki.demo01.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.Resource;

public class MainAbilitySlice extends AbilitySlice {

    Text listenTV,myTV,findTV,accountTV;
    DirectionalLayout dl_frame;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        getWindow().setStatusBarColor(Color.TRANSPARENT.getValue());

        dl_frame = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_frame);
        listenTV = (Text)findComponentById(ResourceTable.Id_listenTV);
        myTV = (Text)findComponentById(ResourceTable.Id_myTV);
        findTV = (Text)findComponentById(ResourceTable.Id_findTV);
        accountTV = (Text)findComponentById(ResourceTable.Id_accountTV);

        //默认第一个
        setUI(new ListenSlice(),listenTV);
        //听吧
        listenTV.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setUI(new ListenSlice(),listenTV);
            }
        });
        //我的
        myTV.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setUI(new MeSlice(),myTV);
            }
        });
        //发现
        findTV.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setUI(new FindSlice(),findTV);
            }
        });
        //账号
        accountTV.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setUI(new AccountSlice(),accountTV);
            }
        });
    }

    public void setUI(Fraction fraction,Text text){
        //清空文字颜色
        setTextColor(text);
        //清空DirectionalLayout内容
        dl_frame.removeAllComponents();
        //加载界面
        ((FractionAbility)getAbility()).getFractionManager().startFractionScheduler().add(ResourceTable.Id_dl_frame,fraction).submit();
    }

    //单个Item切换
    public void setTextColor(Text text){
        ohos.global.resource.ResourceManager resManager = getAbility().getResourceManager();
        try{
            int color = resManager.getElement(ResourceTable.Color_color_d0).getColor();
            int color2 = resManager.getElement(ResourceTable.Color_color_f3).getColor();
            if(text==listenTV){
                Resource resource = resManager.getResource(ResourceTable.Media_ic_home_tab_mine_focused);
                PixelMapElement element = new PixelMapElement(resource);
                listenTV.setAroundElements(null,element,null,null);

                myTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                myTV.setAroundElements(null,element,null,null);

                findTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_discover_normal);
                element = new PixelMapElement(resource);
                findTV.setAroundElements(null,element,null,null);

                accountTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                accountTV.setAroundElements(null,element,null,null);
            }else if(text==myTV){
                Resource resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_focused);
                PixelMapElement element = new PixelMapElement(resource);
                myTV.setAroundElements(null,element,null,null);

                listenTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_mine_normal);
                element = new PixelMapElement(resource);
                listenTV.setAroundElements(null,element,null,null);

                findTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_discover_normal);
                element = new PixelMapElement(resource);
                findTV.setAroundElements(null,element,null,null);

                accountTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                accountTV.setAroundElements(null,element,null,null);
            }else if(text==findTV){
                Resource resource = resManager.getResource(ResourceTable.Media_ic_home_tab_discover_focused);
                PixelMapElement element = new PixelMapElement(resource);
                findTV.setAroundElements(null,element,null,null);

                listenTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_mine_normal);
                element = new PixelMapElement(resource);
                listenTV.setAroundElements(null,element,null,null);

                myTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                myTV.setAroundElements(null,element,null,null);

                accountTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                accountTV.setAroundElements(null,element,null,null);
            }else if(text==accountTV){
                Resource resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_focused);
                PixelMapElement element = new PixelMapElement(resource);
                accountTV.setAroundElements(null,element,null,null);

                listenTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_mine_normal);
                element = new PixelMapElement(resource);
                listenTV.setAroundElements(null,element,null,null);

                myTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_account_normal);
                element = new PixelMapElement(resource);
                myTV.setAroundElements(null,element,null,null);

                findTV.setTextColor(new Color(color));
                resource = resManager.getResource(ResourceTable.Media_ic_home_tab_discover_normal);
                element = new PixelMapElement(resource);
                findTV.setAroundElements(null,element,null,null);
            }
            text.setTextColor(new Color(color2));
        }catch (Exception e){}
    }

    //界面显示执行
    @Override
    protected void onActive() {
        super.onActive();
    }

    //回退拦截
    @Override
    protected void onBackPressed() {
        //弹出提示框
        CommonDialog commonDialog = new CommonDialog(getContext());
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_layout_dialog, null, false);
        commonDialog.setContentCustomComponent(toastLayout);
        //退出应用
        Button dialog_out = (Button)toastLayout.findComponentById(ResourceTable.Id_dialog_out);
        dialog_out.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.hide();
                terminateAbility();
            }
        });
        //隐藏
        Button dialog_hide = (Button)toastLayout.findComponentById(ResourceTable.Id_dialog_hide);
        dialog_hide.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.hide();

            }
        });
        //再听听
        Button dialog_continue = (Button)toastLayout.findComponentById(ResourceTable.Id_dialog_continue);
        dialog_continue.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.hide();
            }
        });
        commonDialog.setSize(800,410);
        commonDialog.setAlignment(LayoutAlignment.CENTER);
        commonDialog.show();
    }

    //界面不可见执行
    @Override
    protected void onBackground() {
        super.onBackground();
    }
}
