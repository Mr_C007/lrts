package com.longki.demo01.slice;

import com.longki.demo01.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

/*
 * 账号界面
 * */
public class AccountSlice extends Fraction {

    ScrollView scroll;
    DependentLayout hide_header;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_ability_account_slice,container,false);

        scroll = (ScrollView) component.findComponentById(ResourceTable.Id_scroll);
        hide_header = (DependentLayout) component.findComponentById(ResourceTable.Id_hide_header);
        //ScrollView滚动监听
        scroll.setScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                //纵向滚动大于80显示标题栏，否则隐藏
                if(i1 > 80){
                    hide_header.setVisibility(Component.VISIBLE);
                }else{
                    hide_header.setVisibility(Component.HIDE);
                }
            }
        });
        return component;
    }
}
