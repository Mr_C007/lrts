package com.longki.demo01.slice;

import com.longki.demo01.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import java.util.Timer;
import java.util.TimerTask;

/*
* 我的界面
* */
public class MeSlice extends Fraction {

    int[] bannerArr = new int[]{ResourceTable.Media_banner01,ResourceTable.Media_banner02};
    DirectionalLayout bannerDL;
    //几秒下一张
    private int delay = 3;
    private int count = 0;
    Timer timer;
    ScrollView bannerScroll;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_ability_me_slice,container,false);
        bannerDL = (DirectionalLayout) component.findComponentById(ResourceTable.Id_bannerDL);
        bannerScroll = (ScrollView) component.findComponentById(ResourceTable.Id_bannerScroll);

        for(int i=0;i<bannerArr.length;i++){
            Component component1 = scatter.parse(ResourceTable.Layout_item_banner,container,false);
            Image img = (Image) component1.findComponentById(ResourceTable.Id_itemImg);
            img.setPixelMap(bannerArr[i]);
            bannerDL.addComponent(component1);
        }
        if(timer == null){
            timer = new Timer();
        }
        timer.schedule(new TimerTask(){
            public void run(){
                delay--;
                if(delay == 0){
                    delay = 3;
                    count++;
                    //更新主UI线程
                    getFractionAbility().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            if(count >= bannerArr.length){
                                count = 0;
                                bannerScroll.fluentScrollXTo(0);
                            }else{
                                bannerScroll.fluentScrollXTo(bannerDL.getWidth());
                            }
                        }
                    });
                }
            }
        },0,1*1000);
        return component;
    }
}
