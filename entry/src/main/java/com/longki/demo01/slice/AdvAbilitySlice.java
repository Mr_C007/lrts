package com.longki.demo01.slice;

import com.longki.demo01.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import java.util.Timer;
import java.util.TimerTask;

public class AdvAbilitySlice extends AbilitySlice {

    //几秒跳转
    private int delay = 4;
    Text text;
    Timer timer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_adv);

        text = (Text) findComponentById(ResourceTable.Id_jump);
        text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                jump();
            }
        });
        timer = new Timer();
        timer.schedule(new TimerTask(){
            public void run(){
                delay--;
                //更新主UI线程
                getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        text.setText(delay+" 跳过");
                    }
                });
                if(delay ==0){
                    jump();
                }
            }
        },0,1*1000);
    }

    public void jump(){
        //关闭Timer
        if(timer != null){
            timer.cancel();
        }
        //跳转界面
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.longki.demo01")
                .withAbilityName("com.longki.demo01.MainAbility")
                .build();
        intent.setOperation(operation);
        startAbility(intent);
        setTransitionAnimation(ResourceTable.Animation_slide_right,ResourceTable.Animation_slide_left);
        terminateAbility();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
